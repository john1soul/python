import turtle

def poly(t,x,y,size,side,color):
	t.speed(300)
	t.pencolor('#000000')
	t.fillcolor(color)
	t.begin_fill()
	for n in range (0,4):
		t.forward(size)
		t.left(360/side)
	t.end_fill()

	turtle.update()

def matrix(t,x,y,):
	# color list
	# "#000000" = black 0
	# "#" = dark green 1
	# "#" = light green 2
	# "#" =  lighter green 3
 	# "#" = yellowish green 4
	# "#" = grey 5
	# "#" = white 6
	cl = ["#000000" , "#1e7210" ,"#58e23b" ,"#98FF9E" ,"#00FF0F" ,
			"#848A7F" ,	"#ffffff" ,"#" ,"#" ,"#" ]
	# design list
	mlist = ["660000000066",
			 "601443541406",
			 "025415123510",
			 "041003400430",
			 "013002100130",
			 "031230013230",
			 "043100001340",
			 "034203105130",
			 "001321433200",
			 "660000000066",
			 "603132234106",
			 "010213321030",
			 "020313131010",
			 "030431321010",
			 "600131211006",
			 "660341131066",
			 "660110032066",
			 "660006600066",]
	print(t,x,y)
	t.width(1)
	for k in range(0,18):
		for h in range(0,12):
			colorstring = mlist[k][h]
			colorint = int(colorstring)
			color = cl[colorint]
			print(x*20,y*20)
			print(" color ",color," ",end="")
			t.penup()
			t.goto(h*20,k*-20)
			t.pendown()
			poly(t,h,k,20,4,color)
			


def johnny():
	turtle.TurtleScreen._RUNNING = True
	turtle.screensize(canvwidth=1000, canvheight=1000, bg=None)
	x = -400; y = 400
	w = turtle.Screen()
	w.clear()
	w.bgcolor("#ffffff")
	t = turtle.Turtle()

	turtle.tracer(0, 0)
	
	matrix(t,x,y)
	
	w.exitonclick()
	
if __name__ == '__main__':
	johnny()

'''
cl = ["#f8f8f8","#e8e8e8" ,"#d8d8d8" ,"#b8b8b8" ,
				"#585858" ,"#383838" ,"#282828" , "#181818" ,
				"#ab4642" ,	"#dc9656" ,"#f79a0e" ,"#538947" ,
				"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]

'''
