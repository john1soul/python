import turtle

def poly(t,x,y,size,side,color):
	t.speed(255)
	t.pencolor('#000000')
	t.fillcolor(color)
	t.begin_fill()
	for n in range (0,4):
		t.forward(size)
		t.left(360/side)
	t.end_fill()

	turtle.update()

def matrix(t,x,y,):
	# color list
	# "#262521" = black 0
	# "#9f0f8c" = dark purple 1 
	# "#c858ca" = light purple 2 
	# "#605858" = light grey 3 
	cl = ["#262521" , "#9f0f8c" ,"#c858ca" ,	"#3c3737" ,"#f79a0e" ,
			"#538947" ,	"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]
	# design list
	mlist = ["03000030",
			 "03300330",
			 "30333303",
			 "00333300",
			 "21233212",
			 "30033003",
			 "03300330",
			 "30000003",]
	print(t,x,y)
	t.width(1)
	for k in range(0,8):
		for h in range(0,8):
			colorstring = mlist[k][h]
			colorint = int(colorstring)
			color = cl[colorint]
			print(x*20,y*20)
			print(" color ",color," ",end="")
			t.penup()
			t.goto(h*20,k*-20)
			t.pendown()
			poly(t,h,k,20,4,color)
			


def main():
	turtle.TurtleScreen._RUNNING = True
	turtle.screensize(canvwidth=1000, canvheight=1000, bg=None)
	x = -400; y = 400
	w = turtle.Screen()
	w.clear()
	w.bgcolor("#ffffff")
	t = turtle.Turtle()

	#turtle.tracer(0, 0)
	
	matrix(t,x,y)
	
	w.exitonclick()
	
if __name__ == '__main__':
	main()

'''
cl = ["#f8f8f8","#e8e8e8" ,"#d8d8d8" ,"#b8b8b8" ,
				"#585858" ,"#383838" ,"#282828" , "#181818" ,
				"#ab4642" ,	"#dc9656" ,"#f79a0e" ,"#538947" ,
				"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]

'''
