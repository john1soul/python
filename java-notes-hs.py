/* Lesson 7 Coding Activity Question 1 */

import java.util.Scanner;
public class U2_L7_Activity_One
{
  public static void main(String[] args)
  {
  int a = Integer.MAX_VALUE;
  a++;
  System.out.println(a);
  int b = Integer.MIN_VALUE;
  b--;
  System.out.println(b);
  }
}

import java.util.Scanner;

public class U2_L7_Activity_Two
{
  public static void main(String[] args)
  {
  
    Scanner scan = new Scanner(System.in);
    Integer x = null;
    Integer y = null;

    System.out.println(x + " " + y);
    System.out.println("Enter values:");
    x = scan.nextInt();
    y = scan.nextInt();

    Double avg = (double)((x + y) / 2.0);
    System.out.println("Average of " + x + " and " + y + " is " + avg);
  }
}

/* Lesson 7 Coding Activity Question 3 */

import java.util.Scanner;
import shapes.*;
 
public class U2_L7_Activity_Three
{
  public static void main(String[] args)
  {
 
    Scanner scan = new Scanner(System.in);
    Integer sides;
    Double length;
    
    System.out.println("Enter number of sides:");
    sides = scan.nextInt();
    System.out.println("Enter side length:");
    length = scan.nextDouble();
    
    RegularPolygon p1 = new RegularPolygon(sides, length);
    RegularPolygon p2 = new RegularPolygon(sides + 1, length * 2);
    System.out.println("The area of a " + p1 + " is: " + p1.getArea());
    System.out.println("The area of a " + p2 + " is: " + p2.getArea());
  }
}


The Math Class

In this lesson we are introducing a very special class: the Math class, which is extremely useful to us when performing calculations in Java. The Math class is part of the java.lang package and includes the methods outlined below (for the AP exam) as well as many others:

Absolute Value - abs(double num)
Powers/Exponents - pow(double base, double exponent)
Random Numbers - random()
Square Root - sqrt(double num) 

What's particularly special about these methods is that they are all static methods. Just like with the constants we saw last lesson these are attached to the class itself.  So to use the random method we just type Math.random(), to use the power method we type  Math.pow(), and to use the square root method we type Math.sqrt(). We don’t have to declare a new Math object to use them (like Math m = new Math(), followed by m.random()). You can get to these methods by just using the class name (Math), and the dot operator.

Math class methods

Math.abs is an overloaded method. The first method of this name takes one double as a parameter and returns the absolute value as a double. The double parameter can be an embedded calculation as shown below (in general a calculation can be used for a double or int parameter).

double x = 9.6;
double y = 2.1;
double dist = Math.abs(y - x);
System.out.println(dist);
> 7.5

The second Math.abs method functions identically except that it uses an int parameter and returns an int value:

int y = -2;
System.out.println(Math.abs(y));
> 2

Math.pow takes two doubles as input and returns the first raised to the second as a double.

int a = 2
int b = 6
double answer1 = Math.pow(a, b);
System.out.println(answer1);
> 64.0

Math.sqrt takes a double as input and returns its square root as a double.

double answer2 = Math.sqrt(answer1);
System.out.println(answer2);
> 8.0

Math.random does not take any input, and returns a random double between 0 and 1.

double answer3 = Math.random();
System.out.println(answer3);
> 0.23487…

Random numbers can be very useful in all sorts of applications, but often we want a random integer in a given range, not a random decimal between 0 and 1. We can transform it like this, where we know the range of numbers and the minimum number:

double ran = Math.random();
int range = 100;
int min = 1;
double answer = (int) (ran * range) + min;
System.out.println(answer);
> 24

The answer above is 24 if we follow the order of operations:

ran = 0.23487…
ran * 100 = 23.487…
(int) 23.487… = 23
23 + 1 = 24

Getting the range, minimum, and order of operations correct are very important. Using range = 100 and min = 1 will give us integers in the range {1 … 100}. If we want numbers in the range 0-100, then we would use range = 101, min = 0. We need the range to extend to 101 because when we cast to an int, Java truncates instead of rounding, so it needs to be possible to get a number which is larger than 100.
public static void main method

When you run a file in a programming environment, it looks for the main method in the class you’re running. In fact, when you have written code in a java file so far, you have actually been writing a method for a particular new class. We don't need an object to be instantiated in order to run this method though: if your code is in a file and class called Lesson_5, you don’t need to have a line that says Lesson_5 example = new Lesson_5(). This is because of the static modifier. With the Math class, we saw that making something static attaches the method to the class itself, so when Java looks for a main method to run, it can find it without an object. We'll be looking much more at creating your own classes and methods in unit 5.
